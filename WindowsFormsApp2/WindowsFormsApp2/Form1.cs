﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Догонялки : Form
    {

        public Bitmap HandlerTexture = Resource1.Handler,
                      TargetTexture = Resource1.Target;

        public Point _targetPosition = new Point();
        public Point _direction = Point.Empty;

        public int _score = 0;
        public int time = 10;
        public int _dif = 0;
        public int under;
        public bool timer = false;


        public Догонялки()
        {
            InitializeComponent();

            SetStyle(ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint, true);

            UpdateStyles();


        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            Refresh();

        }

        public void timer2_Tick(object sender, EventArgs e)
        {
            Random r = new Random();
            timer2.Interval = r.Next(500, 1000);
            _direction.X = r.Next(-1, 2);
            _direction.Y = r.Next(-1, 2);
        }

        public void timer4_Tick(object sender, EventArgs e)
        {
            timer4.Interval = 1000;
            under = time;
            CheckTime();
            timeLabel.Text = "Отсчёт игры в секундах " + time.ToString();
            time -= 1;
            if (time < 0)
            { 
                timeLabel.Text = "0";
            }
        }

        public void CheckTime()
        {
            if (under <= 20)
            {
                AddDif(10);
            }
            else if (under >= 21 & under <= 45)
            {
                AddDif(5);
            }
            else if (under >= 46 & under <= 75)
            {
                AddDif(3);
            }
            else
            {
                AddDif(1);
            }
        }

        public void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            var localPosition = this.PointToClient(Cursor.Position);



            _targetPosition.X += _direction.X * _dif;
            _targetPosition.Y += _direction.Y * _dif;

            if (_targetPosition.X < 0 || _targetPosition.X > 652)
            {
                _direction.X *= -1;
            }

            if (_targetPosition.Y < 0 || _targetPosition.Y > 432)
            {
                _direction.Y *= -1;
            }



            Point between = new Point((localPosition.X - _targetPosition.X), (localPosition.Y - _targetPosition.Y));
            float distance = (float)Math.Sqrt((between.X * between.X) + (between.Y * between.Y));

            if (distance < 40 & time >= 0)
            {
                AddScore(1);
            }
            else
            {
                AddScore(0);
            }

            var handlerRect = new Rectangle(localPosition.X - 32, localPosition.Y - 32, 64, 64);
            var targetRect = new Rectangle(_targetPosition.X - 32, _targetPosition.Y - 32, 64, 64);

            g.DrawImage(TargetTexture, handlerRect);
            g.DrawImage(HandlerTexture, targetRect);

        }

        public void timer3_Tick(object sender, EventArgs e)
        {
            Random rand = new Random();
            timer3.Interval = 900;
            _targetPosition.X = rand.Next(128, 652);
            _targetPosition.Y = rand.Next(128, 432);
        }

        public void btnStop_Click(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            clickedButton.Text = _score.ToString();
            btnStop.Visible = true;
        }

        public void AddScore(int score)
        {
            _score += score;
            scoreLabel.Text = _score.ToString();
        }

        public void AddDif(int dif)
        {
            _dif = dif;
            difLabel.Text = "Сложность " + _dif.ToString();
        }

        

        


    }
}
